'use strict';

var createApp = require('../app/app');
var config = require('../app/config');
var Promise = require('bluebird');
var supertestFactory = require('supertest-as-promised');

Promise.longStackTraces();

before(function ()
{
    var testMode = true;
    this.app = createApp(testMode);
    this.supertest = supertestFactory('http://localhost:' + this.app.server.address().port);
});

after(function ()
{
    this.app.server.close();
});
