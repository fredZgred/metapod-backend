'use strict';

var _ = require('lodash');
var sha1 = require('sha1');
var expect = require('chai').expect;

var sqlGetter = require('../../app/service/sqlGetter');
var tokenTypeEnum = require('../../app/service/enum').tokenType;
var testHelper = require('../testHelper');

describe('Register usecase', function () {

    beforeEach(function () {
        return testHelper.cleanUpMainDB();
    });

    describe('When did not sent signUpData', function () {

        it('should respond with 406 - VALIDATION_FAILURE', function () {

            return this.supertest.post('/api/users/register').expect(406).then(function (response) {
                expect(response).to.have.deep.property('error.text', 'No user data has been received');
            })
        });
    });
    describe('When not all required properties has been sent', function () {

        it('should respond with 406 - VALIDATION_FAILURE', function () {

            return this.supertest.post('/api/users/register').send({
                signUpData: {
                    password: '1'
                }
            }).expect(406).then(function (response) {
                expect(response).to.have.deep.property('error.text', 'Lack of required properties');
            })
        });
    });
    describe('When sent passwords does not match', function () {

        it('should respond with 406 - VALIDATION_FAILURE', function () {

            return this.supertest.post('/api/users/register').send({
                signUpData: {
                    password: '1',
                    nickName: 'a',
                    email: '@'
                }
            }).expect(406).then(function (response) {
                expect(response).to.have.deep.property('error.text', 'Passwords does not match');
            })
        });
    });
    describe('When sent signUpData is valid', function () {

        function shouldCreateInactiveUser() {

            it('should create INACTIVE user', function () {

                return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                    expect(user).to.have.property('active', false);
                });
            });
        }

        function shouldCreateProperToken() {

            it('should create EMAIL_CONFIRMATION token for new user', function () {

                var userId;
                return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                    userId = user.id;
                    return testHelper.getWholeCollectionFromMainDB('tokens');
                }).then(function (tokens) {
                    expect(tokens[0]).to.have.property('userId', userId);
                    expect(tokens[0]).to.have.property('type', tokenTypeEnum.EMAIL_CONFIRMATION);
                });
            });
        }

        describe('but token was not sent', function () {

            beforeEach(function () {

                return this.supertest.post('/api/users/register').send({
                    signUpData: {
                        password: '1',
                        passwordConfirmation: '1',
                        nickName: 'a',
                        email: '@'
                    }
                }).expect(200);
            });
            shouldCreateInactiveUser();
            shouldCreateProperToken();
        });
        describe('and token also has been sent', function () {

            describe('but it was invalid', function () {

                beforeEach(function () {

                    return this.supertest.post('/api/users/register').send({
                        signUpData: {
                            password: '1',
                            passwordConfirmation: '1',
                            nickName: 'a',
                            email: '@'
                        },
                        token: 'invalidToken'
                    }).expect(200);
                });
                shouldCreateInactiveUser();
                shouldCreateProperToken();
            });
            describe('but it had invalid type', function () {

                beforeEach(function () {

                    var self = this;
                    return testHelper.executeSqlInMainDB('INSERT INTO tokens(type) VALUES(\'INVALID_TYPE\');').then(function () {
                        return testHelper.executeSqlInMainDB('SELECT * FROM tokens;');
                    }).then(function (tokens) {
                        return self.supertest.post('/api/users/register').send({
                            signUpData: {
                                password: '1',
                                passwordConfirmation: '1',
                                nickName: 'a',
                                email: '@'
                            },
                            token: tokens[0].value
                        }).expect(200);
                    })
                });
                shouldCreateInactiveUser();
                it('should create EMAIL_CONFIRMATION token for new user', function () {
                    var userId;
                    return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                        userId = user.id;
                        return testHelper.getWholeCollectionFromMainDB('tokens');
                    }).then(function (tokens) {
                        expect(tokens).to.have.lengthOf(2);
                        expect(_.map(tokens, 'userId')).to.contain(userId);
                        expect(_.map(tokens, 'type')).to.contain(tokenTypeEnum.EMAIL_CONFIRMATION);
                        expect(_.map(tokens, 'userId').indexOf(userId)).to.be.equal(_.map(tokens, 'type').indexOf(tokenTypeEnum.EMAIL_CONFIRMATION));
                    });
                });
            });
            describe('and it was valid', function () {

                beforeEach(function () {

                    var self = this;
                    return testHelper.executeSqlInMainDB('INSERT INTO tokens(type) VALUES(\'' + tokenTypeEnum.REGISTRATION_BY_INVITATION + '\');').then(function () {
                        return testHelper.executeSqlInMainDB('SELECT * FROM tokens;');
                    }).then(function (tokens) {
                        return self.supertest.post('/api/users/register').send({
                            signUpData: {
                                password: '1',
                                passwordConfirmation: '1',
                                nickName: 'a',
                                email: '@'
                            },
                            token: tokens[0].value
                        }).expect(200);
                    })
                });
                it('should create ACTIVE user', function () {

                    return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                        expect(user).to.have.property('active', true);
                    });
                });
                it('should create AUTHORIZATION token for new user', function () {

                    var userId;
                    return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                        userId = user.id;
                        return testHelper.getWholeCollectionFromMainDB('tokens');
                    }).then(function (tokens) {
                        expect(tokens).to.have.lengthOf(2);
                        expect(_.map(tokens, 'userId')).to.contain(userId);
                        expect(_.map(tokens, 'type')).to.contain(tokenTypeEnum.AUTHORIZATION);
                        expect(_.map(tokens, 'userId').indexOf(userId)).to.be.equal(_.map(tokens, 'type').indexOf(tokenTypeEnum.AUTHORIZATION));
                    });
                });
            });
        });
        describe('always', function () {

            beforeEach(function () {

                return this.supertest.post('/api/users/register').send({
                    signUpData: {
                        password: '1',
                        passwordConfirmation: '1',
                        nickName: 'a',
                        email: '@'
                    }
                }).expect(200).then(function () {
                    return testHelper.getUserByNickNameFromMainDB('a');
                });
            });
            it('should create password for new user', function () {

                var userId;
                return testHelper.getUserByNickNameFromMainDB('a').then(function (user) {
                    userId = user.id;
                    return testHelper.getWholeCollectionFromMainDB('passwords');
                }).then(function (passwords) {
                    expect(passwords[0]).to.have.property('userId', userId);
                    expect(passwords[0]).to.have.property('password', sha1('1'));
                });
            });
        });
    });
});
