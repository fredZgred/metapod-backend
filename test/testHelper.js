'use strict';

var squel = require('squel');
var fsp = require('fs-promise');

var SQLConverter = require('../app/service/SQLConverter');
var SQLException = require('../app/service/SQLException');
var config = require('../app/config');
var mainDB = require('../app/service/db').mainDB;
var sqlGetter = require('../app/service/sqlGetter');

function executeSqlInMainDB(sql, data)
{
    data = data || [];
    return mainDB.getClient().then(function (db)
    {
        return db.query(sql, data).then(function (result)
        {
            db.done();
            return result.rows;
        });
    });
}

function cleanUpMainDB() {
    return sqlGetter.cleanupSchema({schemaName: 'public', role: 'metapod'}).then(executeSqlInMainDB).then(function ()
    {
        return fsp.readFile(__dirname + '/../seed/metapod.schema.sql', {encoding: 'UTF-8'}).then(executeSqlInMainDB);
    });
}

function getUserByNickNameFromMainDB(nickName) {
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('users').where('nick_name = $1').toString();
        return client.query(query, [nickName]).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getWholeCollectionFromMainDB(collectionName) {
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from(collectionName).toString();
        return client.query(query).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows);
        }).finally(client.done);
    }).catch(SQLException.new);
}

module.exports = {
    executeSqlInMainDB: executeSqlInMainDB,
    cleanUpMainDB: cleanUpMainDB,
    getUserByNickNameFromMainDB: getUserByNickNameFromMainDB,
    getWholeCollectionFromMainDB: getWholeCollectionFromMainDB
};
