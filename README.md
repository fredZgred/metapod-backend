# METAPOD

## TODO
* inviting to metapod
* implement providing own database
* revert registration
* try with **nunjucks** for email templates
* implement search instances

## Setup
You have to run postgresql server locally. You can do it using command:
```
docker-compose up -d
```
or if you want you can always configure your local postgresql to have configuration:
```
role: metapod
password: metapod
database: metapod
```
where metapod is owner of od metapod database 
        
## Seed database
```
npm run seed
```

If you want to seed production then use
```
env PRODUCTION=true npm run seed
```
and of course provide production url in config

## Run app
1. Install dependencies: npm install
1. Run application (one of those below): 
     2. npm start
     2. nodemon
     2. run/debug configuration in IDE (top right corner): 
        - node.js
        - node parameters: /usr/bin/nodemon
