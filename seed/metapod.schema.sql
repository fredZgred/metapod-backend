--- TABLES

CREATE SEQUENCE user_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE token_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE instance_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE invitation_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TYPE enum_database_type AS ENUM ('internal', 'external');

CREATE TABLE users (
    id          bigint DEFAULT nextval('user_id_seq'::regclass) NOT NULL,
    email       character varying(256) NOT NULL,
    nick_name   character varying(256) NOT NULL,
    active      boolean DEFAULT FALSE NOT NULL,
    create_date timestamp without time zone NOT NULL DEFAULT now() NOT NULL,
    UNIQUE (nick_name),
    UNIQUE (email)
);

CREATE TABLE passwords (
    user_id     bigint NOT NULL,
    password    character varying(256) NOT NULL
);

CREATE TABLE tokens (
    id          bigint DEFAULT nextval('token_id_seq'::regclass) NOT NULL,
    user_id     bigint,
    instance_id bigint,
    type        character varying(256) DEFAULT 'authorization' NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    value       character varying(256) DEFAULT md5((date_part('epoch',now())::text) || (random()::text)) NOT NULL,
    UNIQUE (value)
);

CREATE TABLE instances (
    id            bigint DEFAULT nextval('instance_id_seq'::regclass) NOT NULL,
    owner_id      bigint NOT NULL,
    database_url  text NOT NULL,
    schema_name   text NOT NULL,
    role          text NOT NULL,
    database_type enum_database_type DEFAULT 'internal' NOT NULL,
    create_date   timestamp without time zone NOT NULL DEFAULT now() NOT NULL,
    name          character varying(256) NOT NULL
);

CREATE TABLE memberships (
    user_id     bigint NOT NULL,
    instance_id bigint NOT NULL
);

CREATE TABLE invitations (
    id          bigint DEFAULT nextval('invitation_id_seq'::regclass) NOT NULL,
    email       character varying(256) NOT NULL,
    instance_id bigint NOT NULL
);



-- PRIMARY KEYS

ALTER TABLE ONLY users
ADD CONSTRAINT "users_PK" PRIMARY KEY (id);

ALTER TABLE ONLY tokens
ADD CONSTRAINT "tokens_PK" PRIMARY KEY (id);

ALTER TABLE ONLY instances
ADD CONSTRAINT "instances_PK" PRIMARY KEY (id);



-- FOREIGN KEYS

ALTER TABLE ONLY passwords
ADD CONSTRAINT "users_passwords_FK" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY tokens
ADD CONSTRAINT "users_tokens_FK" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY instances
ADD CONSTRAINT "users_instances_FK" FOREIGN KEY (owner_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY tokens
ADD CONSTRAINT "instances_tokens_FK" FOREIGN KEY (instance_id) REFERENCES instances(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY memberships
ADD CONSTRAINT "users_memberships_FK" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY memberships
ADD CONSTRAINT "instances_memberships_FK" FOREIGN KEY (instance_id) REFERENCES instances(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY invitations
ADD CONSTRAINT "instances_invitations_FK" FOREIGN KEY (instance_id) REFERENCES instances(id) ON UPDATE CASCADE ON DELETE CASCADE;
