'use strict';

var Promise = require('bluebird');
var fsp = require('fs-promise');
var db = require('../app/service/db');
var sqlGetter = require('../app/service/sqlGetter');
var config = require('../app/config');
var mainDB = db.mainDB;

var anotherDB;

function executeSql(sql, data)
{
    data = data || [];
    var database = anotherDB || mainDB;
    return database.getClient().then(function (db)
    {
        return db.query(sql, data).then(function (result)
        {
            db.done();
            return result.rows;
        });
    });
}

console.info('Seeding script starts at ' + new Date());

return Promise.resolve().then(function ()
{
    var dbUser = process.env.PRODUCTION ? 'jkxtkrerorsbvi' : 'metapod';
    return sqlGetter.cleanupSchema({schemaName: 'public', role: dbUser}).then(executeSql).then(function ()
    {
        return fsp.readFile(__dirname + '/metapod.schema.sql', {encoding: 'UTF-8'}).then(executeSql);
    }).then(function ()
    {
        return fsp.readFile(__dirname + '/metapod.data.sql', {encoding: 'UTF-8'}).then(function (sql)
        {
            return sql.split('$1').join(config.instanceDBUrl).split('$2').join(dbUser);
        }).then(executeSql);
    }).then(function ()
    {
        console.info('DB metapod: schema restored and data seeded');
    });
}).then(function ()
{
    var dbUser = process.env.PRODUCTION ? 'doijgsxecbgyfq' : 'metapod';
    anotherDB = db.getConnector({databaseUrl: config.instanceDBUrl, schemaName: 'public'});
    return sqlGetter.cleanupSchema({schemaName: 'instance_1000', role: dbUser}).then(executeSql).then(function ()
    {
        anotherDB = db.getConnector({databaseUrl: config.instanceDBUrl, schemaName: 'instance_1000'});
        return sqlGetter.actualSchame().then(executeSql);
    }).then(function ()
    {
        return fsp.readFile(__dirname + '/metapod_instance.data.sql', {encoding: 'UTF-8'}).then(executeSql);
    }).then(function ()
    {
        console.info('DB metapod: schema restored and data seeded');
    });
}).then(function ()
{
    console.info('Seeding script finished at ' + new Date());
    process.exit(0);
}).catch(function (error)
{
    console.error(error && error.stack || error);
    process.exit(1);
});
