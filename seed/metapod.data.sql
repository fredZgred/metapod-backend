INSERT INTO users(id, email, nick_name, active)
VALUES(1000, 'test@user', 'testUser', 'true');

INSERT INTO passwords(user_id, password)
VALUES(1000, '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8');

INSERT INTO instances(id, owner_id, database_url, schema_name, role, name)
VALUES(1000, 1000, '$1', 'instance_1000', '$2', 'default');

INSERT INTO memberships(user_id, instance_id)
VALUES(1000, 1000);
