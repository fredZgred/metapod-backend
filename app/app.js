'use strict';

var express = require('express');
var http = require('http');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var nunjucks = require('nunjucks');

var config = require('./config');

function createApp(testMode)
{
    global.testMode = testMode;

    var app = express();
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json({limit: '2048kb'}));
    // app.use(morgan('dev'));

    nunjucks.configure(__dirname + '/templates', {autoescape: true});

    var server = http.createServer(app);
    server.listen(config.port, function ()
    {
        console.info('Http server listening on port', config.port, '\n');
    });
    require('./REST/routes')(app);

    process.on('uncaughtException', function (err)
    {
        console.error('Uncaught exception');
        console.error(err.message);
        console.error(err.stack);
    });

    return {
        server: server
    };
}

module.exports = createApp;
