'use strict';

var applicationException = require('../service/applicationException');
var business = require('../business/business.container');

function questions(router)
{
    router.route('/api/questions').get(function (request, response)
    {
        business(request).getQuestionsManager().findQuestions().then(function (result)
        {
            response.send(result);
        }).catch(function (err)
        {
            applicationException.errorHandler(err, response);
        });
    });

    router.route('/api/questions/:id').get(function (request, response)
    {
        business(request).getQuestionsManager().findQuestionsById(request.params.id).then(function (result)
        {
            response.send(result);
        }).catch(function (err)
        {
            applicationException.errorHandler(err, response);
        });
    });

    router.route('/api/questions').post(function (request, response)
    {
        business(request).getQuestionsManager().askQuestion(request.body.question).then(function (result)
        {
            response.send(result);
        }).catch(function (err)
        {
            applicationException.errorHandler(err, response);
        });
    });
}

module.exports = questions;
