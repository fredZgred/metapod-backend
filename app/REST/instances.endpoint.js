'use strict';

var applicationException = require('../service/applicationException');
var business = require('../business/business.container');

function instances(router)
{
    router.route('/api/instances').get(function (request, response)
    {
        business(request).getInstancesManager().search().then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });
    
    router.route('/api/instances').post(function (request, response)
    {
        business(request).getInstancesManager().createNew(request.body).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/instances/invitations').get(function (request, response)
    {
        business(request).getInstancesManager().getInvitations(request.body).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });
    
    router.route('/api/instances/current').get(function (request, response)
    {
        business(request).getInstancesManager().requireInstance().then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });
    
    router.route('/api/instances/current/optional').get(function (request, response)
    {
        business(request).getInstancesManager().requireInstance().then(function (result)
        {
            response.send(result);
        }).catch(function ()
        {
            response.status(200).send({});
        });
    });

    router.route('/api/instances/current/members').get(function (request, response)
    {
        business(request).getInstancesManager().getMembers().then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/instances/current/invite').post(function (request, response)
    {
        business(request).getInstancesManager().invite(request.body.email).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/instances/current/database/connect').post(function (request, response)
    {
        business(request).getInstancesManager().connectDatabase(request.body.databaseUrl).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/instances/:id/enter').post(function (request, response)
    {
        business(request).getInstancesManager().enter(request.params.id).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/instances/:id/invitation/accept').post(function (request, response)
    {
        business(request).getInstancesManager().acceptInvitation(request.params.id).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });
}

module.exports = instances;
