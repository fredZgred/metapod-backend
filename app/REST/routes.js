'use strict';

var authenticator = require('../service/authenticator');

function routes(router)
{
    function authentication(request, response, next)
    {
        authenticator.user(request).catch(function ()
        {
            return authenticator.userAndInstance(request);
        }).catch(function ()
        {
            // error handling
        }).finally(function ()
        {
            next();
        });
    }

    router.use(authentication);
    require('./instances.endpoint')(router);
    require('./users.endpoint')(router);
    require('./questions.endpoint')(router);
    require('./answers.endpoint')(router);
}

module.exports = routes;
