'use strict';

var applicationException = require('../service/applicationException');
var business = require('../business/business.container');

function users(router)
{
    router.route('/api/users/me').get(function (request, response)
    {
        business(request).getUsersManager().requireUser().then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/users/me/optional').get(function (request, response)
    {
        business(request).getUsersManager().requireUser().then(function (result)
        {
            response.send(result);
        }).catch(function ()
        {
            response.status(200).send({});
        });
    });

    router.route('/api/users/auth').post(function (request, response)
    {
        business(request).getUsersManager().authenticate(request.body.emailOrNickName, request.body.password, request.body.token).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });

    router.route('/api/users/register').post(function (request, response)
    {
        business(request).getUsersManager().register(request.body.signUpData, request.body.token).then(function (result)
        {
            response.send(result);
        }).catch(function (error)
        {
            applicationException.errorHandler(error, response);
        });
    });
}

module.exports = users;
