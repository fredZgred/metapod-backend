'use strict';

var applicationException = require('../service/applicationException');
var business = require('../business/business.container');

function answers(router)
{
    router.route('/api/answers/:id').get(function (request, response)
    {
        business(request).getAnswersManager().findAnswersByQuestionId(request.params.id).then(function (result)
        {
            response.send(result);
        }).catch(function (err)
        {
            applicationException.errorHandler(err, response);
        });
    });

    router.route('/api/answers/:id').post(function (request, response)
    {
        business(request).getAnswersManager().answerQuestion(request.params.id, request.body).then(function (result)
        {
            response.send(result);
        }).catch(function (err)
        {
            applicationException.errorHandler(err, response);
        });
    });
}

module.exports = answers;
