'use strict';

var Promise = require('bluebird');

var applicationException = require('./applicationException');

function isAuthenticated(context)
{
    return context && context.user;
}

function hasInstanceSelected(context)
{
    return context && context.instance;
}

function isInstanceOwner(context)
{
    return hasInstanceSelected(context) && context.instance.ownerId === context.user.id;
}

function checkPermission(permissionCheckMethod)
{
    return function (context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!permissionCheckMethod(context)) {
                reject(applicationException.new(applicationException.UNAUTHORIZED, 'You are not authorized to view this page'));
            } else {
                resolve();
            }
        });
    };
}

module.exports = {
    requireUser: checkPermission(isAuthenticated),
    requireInstance: checkPermission(hasInstanceSelected),
    requireInstanceOwner: checkPermission(isInstanceOwner)
};
