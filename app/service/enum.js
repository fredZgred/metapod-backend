'use strict';

module.exports = {
    tokenType: {
        AUTHORIZATION: 'authorization',
        AUTHORIZATION_AND_INSTANCE: 'authorization-and-instance',
        EMAIL_CONFIRMATION: 'email-confirmation',
        REGISTRATION_BY_INVITATION: 'registration-by-invitation'
    }, emailType: {}
};
