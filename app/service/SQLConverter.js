'use strict';

var _ = require('lodash');

function convertToUnderscore(string)
{
    return string.split(/(?=[A-Z])/).map(function (elem)
    {
        return elem.toLowerCase();
    }).join('_');
}

function convertToCamelCase(string)
{
    return string.split('_').map(function (ele, index)
    {
        if (index) {
            return ele.charAt(0).toUpperCase() + ele.substr(1);
        }
        return ele;
    }).join('');
}

function convert(data, conversionMethod)
{
    if ('string' === typeof data) {
        return conversionMethod(data);
    }
    if (data instanceof Array) {
        return data.map(function (ele)
        {
            return convert(ele, conversionMethod);
        });
    }
    if (data instanceof Object) {
        var newData = {};
        for (var prop in data) {
            if (!data.hasOwnProperty(prop)) {
                continue;
            }
            newData[conversionMethod(prop)] = data[prop];
        }
        return newData;
    }

    return _.cloneDeep(data);
}

module.exports = {
    toCamelCase: function (data)
    {
        return convert(data, convertToCamelCase);
    },
    toUnderscore: function (data)
    {
        return convert(data, convertToUnderscore);
    }
};
