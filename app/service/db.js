'use strict';

var Promise = require('bluebird');
var pg = require('pg');
var jiffy = require('./jiffy');
var config = require('../config');

var oldParser = pg.types.getTypeParser(1114);
pg.types.setTypeParser(1114, function(stringValue)
{
    return jiffy.moment(oldParser(stringValue)).utcOffset(0).toString();
});

function Connector(databaseUrl, schemaName)
{
    this.getClient = function ()
    {
        var pgConnect = Promise.promisify(pg.connect, {context: pg, multiArgs: true});
        return pgConnect(databaseUrl).then(function (args)
        {
            return {
                query: Promise.promisify(args[0].query, {context: args[0]}),
                done: args[1]
            };
        }).then(function (client)
        {
            var sql = 'SET search_path TO ' + schemaName;
            return client.query(sql).then(function ()
            {
                return client;
            });
        });
    };
}

function getConnector(instance)
{
    return new Connector(instance.databaseUrl, instance.schemaName);
}

module.exports = {
    mainDB: getConnector({databaseUrl: config.mainDBUrl, schemaName: 'public'}),
    getConnector: getConnector
};
