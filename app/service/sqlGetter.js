'use strict';

var fsp = require('fs-promise');

var sql = {};
var promise;

(function init()
{
    promise = fsp.readFile(__dirname + '/../sql/cleanup-schema.sql', {encoding: 'UTF-8'}).then(function (result)
    {
        sql.cleanupSchema = result;
        return fsp.readFile(__dirname + '/../sql/metapod_instance.schema.sql', {encoding: 'UTF-8'});
    }).then(function (result)
    {
        sql.instanceSchema = result;
    });
})();

module.exports = {
    cleanupSchema: function (instance)
    {
        return promise.then(function ()
        {
            return sql.cleanupSchema.split('$1').join(instance.schemaName).split('$2').join(instance.role);
        });
    },
    actualSchame: function ()
    {
        return promise.then(function ()
        {
            return sql.instanceSchema;
        });
    }
};
