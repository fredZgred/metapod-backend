'use strict';

var Promise = require('bluebird');

var business = require('../business/business.container.js');

function user(data)
{
    if (data.headers.authorization) {
        return business(data).getTokenManager().getUserByToken(data.headers.authorization).then(function (result)
        {
            data.user = result;
            return data;
        });
    }
    return Promise.resolve(data);
}

function userAndInstance(data)
{
    if (data.headers.authorization) {
        return business(data).getTokenManager().getUserAndInstanceByToken(data.headers.authorization).then(function (result)
        {
            data.user = result.user;
            data.instance = result.instance;
            return data;
        });
    }
    return Promise.resolve(data);
}

module.exports = {
    user: user,
    userAndInstance: userAndInstance
};
