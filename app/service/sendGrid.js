'use strict';

var Promise = require('bluebird');
var sg = require('sendgrid');
var nunjucks = require('nunjucks');

var config = require('../config');
var sendGrid = sg(config.sendGrid.apiKey);
var sendEmailMethod = Promise.promisify(sendGrid.send, {context: sendGrid});

function sendEmail(email)
{
    if (!global.testMode) {
        return sendEmailMethod(email);
    }
}

function inviteNewUser(email, emailAlreadyRegistered, inviter, metapodName, token)
{
    var html = nunjucks.render('invitation.html', {
        emailAlreadyRegistered: emailAlreadyRegistered,
        invitationsLink: config.frontEndUrl + '/dashboard',
        registrationLink: config.frontEndUrl + '/sign-up?email=' + email + (token ? '&token=' + token.token : ''),
        inviter: inviter,
        metapodName: metapodName
    });
    var emailPayload = {
        to: email,
        from: config.sendGrid.noReplyAddress,
        fromname: config.applicationName,
        subject: 'You have been invited to metapod',
        html: html
    };
    return sendEmail(new sendGrid.Email(emailPayload));
}

function confirmEmail(email, token)
{
    var html = nunjucks.render('registration.html', {
        metapodUrl: config.frontEndUrl,
        buttonUrl: config.frontEndUrl + '/sign-in?token=' + token.token
    });
    var emailPayload = {
        to: email,
        from: config.sendGrid.noReplyAddress,
        fromname: config.applicationName,
        subject: 'Confirm email',
        html: html
    };
    return sendEmail(new sendGrid.Email(emailPayload));
}

module.exports = {
    inviteNewUser: inviteNewUser,
    confirmEmail: confirmEmail
};
