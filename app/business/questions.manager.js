'use strict';

var questionsDAO = require('../DAO/questionsDAO');
var applicationException = require('../service/applicationException');
var security = require('../service/security');

function create(context, business)
{
    function findQuestions()
    {
        // TODO: implement find which takes string and returns questions containing it in title
        return security.requireInstance(context).then(function ()
        {
            return questionsDAO.asInstance(context.instance).find();
        });
    }

    function findQuestionsById(questionId)
    {
        // TODO: implement find which takes string and returns questions containing it in title
        return security.requireInstance(context).then(function ()
        {
            return questionsDAO.asInstance(context.instance).findById(questionId);
        });
    }

    function askQuestion(question)
    {
        return security.requireInstance(context).then(function ()
        {
            question.ownerId = context.user.id;
            return questionsDAO.asInstance(context.instance).create(question).catch(function ()
            {
                // TODO: add error handling
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'Question does not have required fields');
            });
        });
    }

    return {
        findQuestions: findQuestions,
        findQuestionsById: findQuestionsById,
        askQuestion: askQuestion
    };
}

module.exports = {
    create: create
};
