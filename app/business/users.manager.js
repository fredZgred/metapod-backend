'use strict';

var Promise = require('bluebird');
var sha1 = require('sha1');
var md5 = require('md5');
var _ = require('lodash');

var usersDAO = require('../DAO/usersDAO');
var passwordsDAO = require('../DAO/passwordsDAO');
var tokensDAO = require('../DAO/tokensDAO');
var tokenTypeEnum = require('../service/enum').tokenType;
var applicationException = require('../service/applicationException');
var security = require('../service/security');
var sendGrid = require('../service/sendGrid');

function create(context, business)
{
    function hashPassword(password)
    {
        return sha1(password);
    }

    function requireUser()
    {
        return security.requireUser(context).then(function ()
        {
            return _.assign({}, context.user, {email: void 0, emailHash: md5(context.user.email)});
        });
    }

    function checkToken(tokenValue, tokenType)
    {
        return tokensDAO.getByValue(tokenValue).then(function (tokenObj)
        {
            if (tokenType === tokenObj.type) {
                return true;
            }
        }).catch(function ()
        {
            return false;
        });
    }

    function checkWhetherUserCanBeAuthenticated(user, tokenValue)
    {
        return Promise.resolve().then(function ()
        {
            if (user.active) {
                return true;
            }
            if (tokenValue) {
                return checkToken(tokenValue, tokenTypeEnum.EMAIL_CONFIRMATION).then(function (isTokenValid)
                {
                    if (isTokenValid) {
                        return usersDAO.activate(user.id).then(function ()
                        {
                            return true;
                        });
                    }
                });
            }
        }).then(function (isUserActive)
        {
            if (!isUserActive) {
                throw applicationException.new(applicationException.UNAUTHORIZED,
                        'User inactive. Find confirmation email in your inbox and active this account.');
            }
        });
    }

    function authenticate(emailOrNickName, password, token)
    {
        var foundUser;
        return usersDAO.getByEmailOrNickName(emailOrNickName).then(function (user)
        {
            foundUser = user;
            if (!foundUser) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'User not found');
            }

            return checkWhetherUserCanBeAuthenticated(user, token);
        }).then(function ()
        {
            return passwordsDAO.authorize(foundUser.id, hashPassword(password)).then(function ()
            {
                return business.getTokenManager().createNew({
                    userId: foundUser.id
                }, tokenTypeEnum.AUTHORIZATION);
            });
        }).catch(function (err)
        {
            if (applicationException.is(err, applicationException.NOT_FOUND)) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'User does not exist');
            } else {
                throw err;
            }
        });
    }

    function register(userData, token)
    {
        var registeredUser;
        var isEmailConfirmed;
        return Promise.resolve().then(function ()
        {
            if (!userData) {
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'No user data has been received');
            }
            if (!userData.email || !userData.nickName || !userData.password) {
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'Lack of required properties')
            }
            if (userData.password !== userData.passwordConfirmation) {
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'Passwords does not match');
            }
        }).then(function ()
        {
            if (token) {
                return checkToken(token, tokenTypeEnum.REGISTRATION_BY_INVITATION).then(function (isTokenValid)
                {
                    isEmailConfirmed = isTokenValid;
                });
            }
        }).then(function ()
        {
            var userEntity = _.assign(_.pick(userData, ['email', 'nickName']), {active: isEmailConfirmed || false});
            return usersDAO.createNew(userEntity);
        }).then(function (user)
        {
            registeredUser = user;
            return passwordsDAO.createNew(registeredUser.id, hashPassword(userData.password));
        }).then(function ()
        {
            var tokenIds = {
                userId: registeredUser.id
            };
            var tokenType = isEmailConfirmed ? tokenTypeEnum.AUTHORIZATION : tokenTypeEnum.EMAIL_CONFIRMATION;
            return business.getTokenManager().createNew(tokenIds, tokenType);
        }).then(function (newToken)
        {
            if (isEmailConfirmed) {
                return newToken;
            }

            return sendGrid.confirmEmail(registeredUser.email, newToken);
        });
    }

    return {
        requireUser: requireUser,
        authenticate: authenticate,
        register: register
    };
}

module.exports = {
    create: create
};
