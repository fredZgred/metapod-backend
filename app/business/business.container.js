'use strict';

var _ = require('lodash');

var instancesManager = require('./instances.manager');
var tokenManager = require('./token.manager');
var usersManager = require('./users.manager');
var questionsManager = require('./questions.manager');
var answersManager = require('./answers.manager');

function getContext(request)
{
    return {
        user: request && request.user,
        instance: request && request.instance
    };
}

function getter(manager, request)
{
    return function ()
    {
        return manager.create(getContext(request), this);
    };
}

module.exports = function createBusinessContainer(request)
{
    return {
        getInstancesManager: getter(instancesManager, request),
        getTokenManager: getter(tokenManager, request),
        getUsersManager: getter(usersManager, request),
        getQuestionsManager: getter(questionsManager, request),
        getAnswersManager: getter(answersManager, request),

        asUser: function (user)
        {
            return createBusinessContainer(_.extend({}, getContext(request), {user: user}));
        }
    };
};
