'use strict';

var answersDAO = require('../DAO/answersDAO');
var applicationException = require('../service/applicationException');
var security = require('../service/security');

function create(context, business)
{
    function findAnswersByQuestionId(questionId)
    {
        return security.requireInstance(context).then(function ()
        {
            return answersDAO.asInstance(context.instance).findByQuestionId(questionId);
        });
    }

    function answerQuestion(questionId, answer)
    {
        return security.requireInstance(context).then(function ()
        {
            answer.ownerId = context.user.id;
            answer.questionId = questionId;
            return answersDAO.asInstance(context.instance).create(answer).catch(function ()
            {
                // TODO: add error handling
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'Question does not have required fields');
            });
        });
    }

    return {
        findAnswersByQuestionId: findAnswersByQuestionId,
        answerQuestion: answerQuestion
    };
}

module.exports = {
    create: create
};
