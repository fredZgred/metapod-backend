'use strict';

var md5 = require('md5');
var _ = require('lodash');

var instancesDAO = require('../DAO/instancesDAO');
var usersDAO = require('../DAO/usersDAO');
var applicationException = require('../service/applicationException');
var security = require('../service/security');
var sendGrid = require('../service/sendGrid');
var tokenTypeEnum = require('../service/enum').tokenType;

function create(context, business)
{
    function search()
    {
        return security.requireUser(context).then(function ()
        {
            return instancesDAO.getAllWhichUserBelongsTo(context.user.id);
        });
    }

    function createNew(instanceData)
    {
        var newInstance;
        return security.requireUser(context).then(function ()
        {
            if (!instanceData.name) {
                throw applicationException.new(applicationException.VALIDATION_FAILURE, 'You have to specify name of new metapod');
            }

            instanceData.ownerId = context.user.id;
            return instancesDAO.create(instanceData);
        }).then(function (instance)
        {
            newInstance = instance;
            return instancesDAO.setAsMember(context.user.id, newInstance.id);
        }).then(function ()
        {
            return enter(newInstance.id);
        });
    }

    function requireInstance()
    {
        return security.requireInstance(context).then(function ()
        {
            return _.assign({}, context.instance, {
                databaseUrl: 'external' === context.instance.databaseType ? context.instance.databaseUrl : void 0,
                schemaName: void 0
            });
        });
    }

    function getInvitations()
    {
        return security.requireUser(context).then(function ()
        {
            return instancesDAO.getInvitations(context.user.email);
        }).then(function (invitations)
        {
            return _.map(invitations, function (invitation)
            {
                return _.pick(invitation, ['id', 'name']);
            });
        });
    }

    function getMembers()
    {
        return security.requireInstanceOwner(context).then(function ()
        {
            return instancesDAO.getMembers(context.instance.id);
        }).then(function (members)
        {
            return _.map(members, function (member)
            {
                return _.assign({}, _.pick(member, ['id', 'nickName']), {emailHash: md5(member.email)});
            });
        });
    }

    function invite(email)
    {
        var emailAlreadyRegistered;
        return security.requireInstanceOwner(context).then(function ()
        {
            return instancesDAO.invite(email, context.instance.id);
        }).then(function ()
        {
            return usersDAO.getByEmail(email).then(function ()
            {
                emailAlreadyRegistered = true;
            }).catch(function (error)
            {
                if (applicationException.is(error, applicationException.NOT_FOUND)) {
                    emailAlreadyRegistered = false;
                } else {
                    throw error;
                }
            });
        }).then(function ()
        {
            if (!emailAlreadyRegistered) {
                return business.getTokenManager().createNew({}, tokenTypeEnum.REGISTRATION_BY_INVITATION);
            }
        }).then(function (token)
        {
            return sendGrid.inviteNewUser(email, emailAlreadyRegistered, context.user.nickName, context.instance.name, token);
        });
    }

    function enter(instanceId)
    {
        return security.requireUser(context).then(function ()
        {
            return instancesDAO.checkMembership(instanceId, context.user.id);
        }).then(function ()
        {
            return business.getTokenManager().createNew({
                userId: context.user.id,
                instanceId: instanceId
            }, tokenTypeEnum.AUTHORIZATION_AND_INSTANCE);
        });
    }

    function acceptInvitation(instanceId)
    {
        return security.requireUser(context).then(function ()
        {
            return instancesDAO.checkAndRemoveInvitation(instanceId, context.user.email);
        }).then(function ()
        {
            return instancesDAO.setAsMember(context.user.id, instanceId);
        });
    }

    function connectDatabase(databaseUrl)
    {
        return security.requireInstanceOwner(context).then(function ()
        {
            return instancesDAO.changeDatabase(context.instance, databaseUrl);
        });
    }

    return {
        search: search,
        createNew: createNew,
        requireInstance: requireInstance,
        getInvitations: getInvitations,
        getMembers: getMembers,
        invite: invite,
        enter: enter,
        acceptInvitation: acceptInvitation,
        connectDatabase: connectDatabase
    };
}

module.exports = {
    create: create
};
