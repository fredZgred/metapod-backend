'use strict';

var sha1 = require('sha1');

var usersDAO = require('../DAO/usersDAO');
var instancesDAO = require('../DAO/instancesDAO');
var tokensDAO = require('../DAO/tokensDAO');
var tokenTypeEnum = require('../service/enum').tokenType;
var applicationException = require('../service/applicationException');

function create(context, business)
{
    function getToken(createTokenPromise)
    {
        return createTokenPromise.then(function (result)
        {
            return {token: result.value};
        });
    }

    function createNew(ids, type)
    {
        return getToken(tokensDAO.create(ids, type));
    }

    function getUserByToken(token)
    {
        return tokensDAO.getByValue(token).then(function (tokenObj)
        {
            if (tokenTypeEnum.AUTHORIZATION !== tokenObj.type) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'Fake token');
            }

            return usersDAO.get(tokenObj.userId);
        });
    }

    function getUserAndInstanceByToken(token)
    {
        var result = {};
        return tokensDAO.getByValue(token).then(function (tokenObj)
        {
            if (tokenTypeEnum.AUTHORIZATION_AND_INSTANCE !== tokenObj.type) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'Fake token');
            }

            token = tokenObj;
            return instancesDAO.get(token.instanceId);
        }).then(function (instance)
        {
            result.instance = instance;
            return usersDAO.get(token.userId);
        }).then(function (user)
        {
            result.user = user;
            return result;
        });
    }

    return {
        createNew: createNew,
        getUserByToken: getUserByToken,
        getUserAndInstanceByToken: getUserAndInstanceByToken
    };
}

module.exports = {
    create: create
};
