CREATE SEQUENCE question_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE tag_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE answer_id_seq
    START WITH 1001
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE members (
    id          bigint NOT NULL,
    UNIQUE(id)
);

CREATE TABLE questions (
    id          bigint DEFAULT nextval('question_id_seq'::regclass) NOT NULL,
    owner_id    bigint NOT NULL,
    title       character varying(256) NOT NULL,
    body        text NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    update_date timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE tags (
    id          bigint DEFAULT nextval('tag_id_seq'::regclass) NOT NULL,
    name        character varying(30) NOT NULL
);

CREATE TABLE questions_tags (
    question_id bigint NOT NULL,
    tag_id      bigint NOT NULL
);

CREATE TABLE answers (
    id          bigint DEFAULT nextval('answer_id_seq'::regclass) NOT NULL,
    question_id bigint NOT NULL,
    owner_id    bigint NOT NULL,
    body        text NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    update_date timestamp without time zone DEFAULT now() NOT NULL
);


-- PRIMARY KEYS

ALTER TABLE ONLY members
ADD CONSTRAINT "member_PK" PRIMARY KEY (id);

ALTER TABLE ONLY questions
ADD CONSTRAINT "question_PK" PRIMARY KEY (id);

ALTER TABLE ONLY tags
ADD CONSTRAINT "tag_PK" PRIMARY KEY (id);

ALTER TABLE ONLY answers
ADD CONSTRAINT "answer_PK" PRIMARY KEY (id);

-- FOREIGN KEYS

ALTER TABLE ONLY questions
ADD CONSTRAINT "member_question_FK" FOREIGN KEY (owner_id) REFERENCES members(id) ON UPDATE CASCADE;

ALTER TABLE ONLY questions_tags
ADD CONSTRAINT "question_FK" FOREIGN KEY (question_id) REFERENCES questions(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY questions_tags
ADD CONSTRAINT "tag_FK" FOREIGN KEY (tag_id) REFERENCES tags(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY answers
ADD CONSTRAINT "question_FK" FOREIGN KEY (question_id) REFERENCES questions(id) ON UPDATE CASCADE;

ALTER TABLE ONLY answers
ADD CONSTRAINT "member_answer_FK" FOREIGN KEY (owner_id) REFERENCES members(id) ON UPDATE CASCADE;
