'use strict';

var squel = require('squel');

var applicationException = require('../service/applicationException');
var SQLConverter = require('../service/SQLConverter');
var SQLException = require('../service/SQLException');
var tokenTypeEnum = require('../service/enum').tokenType;
var mainDB = require('../service/db').mainDB;

function create(ids, type)
{
    if (!type) {
        throw applicationException.new(applicationException.PRECONDITION_FAILED);
    }
    var tokenData = {
        type: type
    };
    switch (type) {
        case tokenTypeEnum.AUTHORIZATION:
            tokenData[SQLConverter.toUnderscore('userId')] = ids.userId;
            break;
        case tokenTypeEnum.AUTHORIZATION_AND_INSTANCE:
            tokenData[SQLConverter.toUnderscore('userId')] = ids.userId;
            tokenData[SQLConverter.toUnderscore('instanceId')] = ids.instanceId;
            break;
        case tokenTypeEnum.EMAIL_CONFIRMATION:
            tokenData[SQLConverter.toUnderscore('userId')] = ids.userId;
            break;
    }
    return mainDB.getClient().then(function (client)
    {
        var query = squel.insert().into('tokens').setFields(tokenData).toString();
        query += 'RETURNING *';
        return client.query(query).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getByValue(tokenValue)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('tokens').where('value = $1').toString();
        return client.query(query, [tokenValue]).then(function (result)
        {
            if (1 === result.rowCount) {
                return SQLConverter.toCamelCase(result.rows[0]);
            }

            throw applicationException.new(applicationException.NOT_FOUND, 'Token not found');
        }).finally(client.done);
    }).catch(SQLException.new);
}

module.exports = {
    create: create,
    getByValue: getByValue
};
