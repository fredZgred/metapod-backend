'use strict';

var squel = require('squel');

var applicationException = require('../service/applicationException');
var SQLException = require('../service/SQLException');
var SQLConverter = require('../service/SQLConverter');
var mainDB = require('../service/db').mainDB;

function authorize(userId, password)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('passwords').where('user_id = $1').where('password = $2').toString();
        return client.query(query, [userId, password]).then(function (result)
        {
            if (1 === result.rowCount) {
                return true;
            }

            throw applicationException.new(applicationException.UNAUTHORIZED, 'User and password does not match');
        }).finally(client.done);
    }).catch(SQLException.new);
}

function createNew(userId, password)
{
    return mainDB.getClient().then(function (client)
    {
        var passwordEntity = {
            userId: userId,
            password: password
        };
        var query = squel.insert().into('passwords').setFields(SQLConverter.toUnderscore(passwordEntity)).toString();
        return client.query(query).finally(client.done);
    }).catch(SQLException.new);
}

module.exports = {
    authorize: authorize,
    createNew: createNew
};
