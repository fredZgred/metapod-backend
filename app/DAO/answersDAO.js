'use strict';

var squel = require('squel');

var applicationException = require('../service/applicationException');
var SQLConverter = require('../service/SQLConverter');
var SQLException = require('../service/SQLException');
var db = require('../service/db');
var mainDB = db.mainDB;

function asInstance(instance)
{
    if (!instance || !instance.databaseUrl || !instance.schemaName) {
        throw new Error('Invalid instance');
    }

    function create(answer)
    {
        return db.getConnector(instance).getClient().then(function (client)
        {
            var answerData = SQLConverter.toUnderscore(answer);
            var query = squel.insert().into('answers').setFields(answerData).toString();
            query += 'RETURNING *';

            return client.query(query).then(function (result)
            {
                return SQLConverter.toCamelCase(result.rows[0]);
            }).finally(client.done);
        }).catch(SQLException.new);
    }

    function findByQuestionId(id)
    {
        return db.getConnector(instance).getClient().then(function (client)
        {
            var condition = "question_id = '" + id + "'";
            var query = squel.select().from('answers').where(condition).order('create_date').toString();
            return client.query(query).then(function (result)
            {
                if (0 === result.rowCount) {
                    throw applicationException.new(applicationException.NOT_FOUND, 'Answers for question ' + id + ' not found.');
                }

                return SQLConverter.toCamelCase(result.rows);
            }).finally(client.done);
        }).catch(SQLException.new);
    }

    return {
        create: create,
        findByQuestionId: findByQuestionId
    };
}

module.exports = {
    asInstance: asInstance
};
