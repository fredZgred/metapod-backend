'use strict';

var squel = require('squel');

var applicationException = require('../service/applicationException');
var SQLConverter = require('../service/SQLConverter');
var SQLException = require('../service/SQLException');
var db = require('../service/db');
var mainDB = db.mainDB;

function asInstance(instance)
{
    if (!instance || !instance.databaseUrl || !instance.schemaName) {
        throw new Error('Invalid instance');
    }

    function create(question)
    {
        return db.getConnector(instance).getClient().then(function (client)
        {
            var questionData = SQLConverter.toUnderscore(question);
            var query = squel.insert().into('questions').setFields(questionData).toString();
            query += 'RETURNING *';

            return client.query(query).then(function (result)
            {
                return SQLConverter.toCamelCase(result.rows[0]);
            }).finally(client.done);
        }).catch(SQLException.new);
    }

    function find()
    {
        return db.getConnector(instance).getClient().then(function (client)
        {
            var query = squel.select().from('questions').order('create_date', false).toString();
            return client.query(query).then(function (result)
            {
                if (0 === result.rowCount) {
                    throw applicationException.new(applicationException.NOT_FOUND, 'Questions not found.');
                }

                return SQLConverter.toCamelCase(result.rows);
            }).finally(client.done);
        }).catch(SQLException.new);
    }

    function findById(id)
    {
        return db.getConnector(instance).getClient().then(function (client)
        {
            var condition = "id = '" + id + "'";
            var query = squel.select().from('questions').where(condition).toString();
            return client.query(query).then(function (result)
            {
                if (0 === result.rowCount) {
                    throw applicationException.new(applicationException.NOT_FOUND, 'Question ' + id + ' not found.');
                }

                return SQLConverter.toCamelCase(result.rows[0]);
            }).finally(client.done);
        }).catch(SQLException.new);
    }

    return {
        create: create,
        find: find,
        findById: findById
    };
}

module.exports = {
    asInstance: asInstance
};
