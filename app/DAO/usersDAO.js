'use strict';

var squel = require('squel');

var applicationException = require('../service/applicationException');
var SQLConverter = require('../service/SQLConverter');
var SQLException = require('../service/SQLException');
var mainDB = require('../service/db').mainDB;

function createNew(userData)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('users').where('email = $1 OR nick_name ILIKE $2').toString();
        return client.query(query, [userData.email, userData.nickName]).then(function (result)
        {
            if (!result.rowCount) {
                return true;
            }

            var message = result.rows[0].email === userData.email ? 'Email already registered' : 'Nick name already exists';
            throw applicationException.new(applicationException.VALIDATION_FAILURE, message);
        }).then(function ()
        {
            userData = SQLConverter.toUnderscore(userData);
            var query = squel.insert().into('users').setFields(userData).toString();
            query += 'RETURNING *';
            return client.query(query).then(function (result)
            {
                if (!result.rowCount) {
                    throw applicationException.new(applicationException.NOT_FOUND);
                }
                return SQLConverter.toCamelCase(result.rows[0]);
            });
        }).finally(client.done);
    }).catch(SQLException.new);
}

function get(userId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('users').where('id = $1').toString();
        return client.query(query, [userId]).then(function (result)
        {
            if (!result.rowCount) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getByEmail(userEmail)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('users').where('email = $1').toString();
        return client.query(query, [userEmail]).then(function (result)
        {
            if (!result.rowCount) {
                throw applicationException.new(applicationException.NOT_FOUND, 'User not found');
            }
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getByEmailOrNickName(emailOrNickName)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('users').where('email = $1 OR nick_name ILIKE $2').toString();
        return client.query(query, [emailOrNickName, emailOrNickName]).then(function (result)
        {
            if (!result.rowCount) {
                throw applicationException.new(applicationException.NOT_FOUND, 'User not found');
            }
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function activate(userId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.update().table('users').set('active', 'true').where('id = $1').toString();
        return client.query(query, [userId]).finally(client.done);
    }).catch(SQLException.new);
}

module.exports = {
    createNew: createNew,
    get: get,
    getByEmail: getByEmail,
    getByEmailOrNickName: getByEmailOrNickName,
    activate: activate
};
