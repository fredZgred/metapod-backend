'use strict';

var squel = require('squel');
var Promise = require('bluebird');
var _ = require('lodash');

var applicationException = require('../service/applicationException');
var SQLConverter = require('../service/SQLConverter');
var SQLException = require('../service/SQLException');
var sqlGetter = require('../service/sqlGetter');
var config = require('../config');
var db = require('../service/db');
var mainDB = db.mainDB;

function get(instanceId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('instances').where('id = $1').toString();
        return client.query(query, [instanceId]).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows[0]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getRoleFromUrl(dbUrl)
{
    return dbUrl.split('//')[1].split(':')[0];
}

function create(instanceData)
{
    var query;
    var instance;
    return mainDB.getClient().then(function (client)
    {
        return Promise.resolve().then(function ()
        {
            query = squel.select('last_value').from('instance_id_seq').toString();
            return client.query(query).then(function (result)
            {
                /* jshint -W106 */
                return 'instance_' + (parseInt(result.rows[0].last_value, 10) + 1);
            });
        }).then(function (newInstanceSchema)
        {
            instanceData.databaseUrl = config.instanceDBUrl;
            instanceData.schemaName = newInstanceSchema;
            instanceData.role = getRoleFromUrl(config.instanceDBUrl);
            instanceData = SQLConverter.toUnderscore(instanceData);
            query = squel.insert().into('instances').setFields(instanceData).toString();
            query += 'RETURNING *';
            return client.query(query).then(function (result)
            {
                instance = SQLConverter.toCamelCase(result.rows[0]);
            });
        }).finally(client.done);
    }).then(function ()
    {
        return db.getConnector(_.assign({}, instance, {schemaName: 'public'})).getClient();
    }).then(function (client)
    {
        return sqlGetter.cleanupSchema(instance).then(function (cleanupSchemaQuery)
        {
            return client.query(cleanupSchemaQuery).finally(client.done);
        });
    }).then(function ()
    {
        return db.getConnector(instance).getClient();
    }).then(function (client)
    {
        return sqlGetter.actualSchame().then(function (schemaStructure)
        {
            return client.query(schemaStructure).finally(client.done);
        });
    }).then(function ()
    {
        return instance;
    }).catch(SQLException.new);
}

function setAsMember(userId, instanceId)
{
    return mainDB.getClient().then(function (client)
    {
        var membership = {
            userId: userId,
            instanceId: instanceId
        };
        var query = squel.insert().into('memberships').setFields(SQLConverter.toUnderscore(membership)).toString();
        return client.query(query).finally(client.done);
    }).catch(SQLException.new).then(function ()
    {
        return get(instanceId);
    }).then(function (instance)
    {
        return db.getConnector(instance).getClient();
    }).then(function (client)
    {
        var query = squel.insert().into('members').setFields({id: userId}).toString();
        return client.query(query);
    }).catch(SQLException.new);
}

function invite(email, instanceId)
{
    return mainDB.getClient().then(function (client)
    {
        var invitation = SQLConverter.toUnderscore({
            email: email,
            instanceId: instanceId
        });
        var query = squel.insert().into('invitations').setFields(invitation).toString();
        return client.query(query).finally(client.done);
    }).catch(SQLException.new);
}

function checkMembership(instanceId, userId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select().from('memberships').where('user_id = $1 AND instance_id = $2').toString();
        return client.query(query, [userId, instanceId]).then(function (result)
        {
            if (1 !== result.rowCount) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'Can not resolve membership');
            }
        }).finally(client.done);
    }).then(function ()
    {
        return get(instanceId);
    }).then(function (instance)
    {
        return db.getConnector(instance).getClient();
    }).then(function (client)
    {
        var query = squel.select().from('members').where('id = $1').toString();
        return client.query(query, [userId]).then(function (result)
        {
            if (1 === result.rowCount) {
                return true;
            }

            throw applicationException.new(applicationException.UNAUTHORIZED, 'Can not resolve membership');
        });
    }).catch(SQLException.new);
}

function getAllWhichUserBelongsTo(userId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select('instances.*')
                .from('memberships')
                .join('instances', null, 'memberships.instance_id = instances.id')
                .where('user_id = $1')
                .toString();
        return client.query(query, [userId]).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getMembers(instanceId)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select('users.*')
                .from('memberships')
                .join('users', null, 'memberships.user_id = users.id')
                .where('instance_id = $1')
                .toString();
        return client.query(query, [instanceId]).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function getInvitations(email)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select('instances.*')
                .from('invitations')
                .join('instances', null, 'invitations.instance_id = instances.id')
                .where('email = $1')
                .toString();
        return client.query(query, [email]).then(function (result)
        {
            return SQLConverter.toCamelCase(result.rows);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function checkAndRemoveInvitation(instanceId, email)
{
    return mainDB.getClient().then(function (client)
    {
        var query = squel.select()
                .from('invitations')
                .where('email = $1 AND instance_id = $2')
                .toString();
        return client.query(query, [email, instanceId]).then(function (result)
        {
            if (!result.rowCount) {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Can not accept invitation');
            }

            var query = squel.delete()
                    .from('invitations')
                    .where('email = $1 AND instance_id = $2')
                    .toString();
            return client.query(query, [email, instanceId]);
        }).finally(client.done);
    }).catch(SQLException.new);
}

function changeDatabase(instance, databaseUrl)
{
    var dump;
    var newDBData = {
        databaseUrl: databaseUrl + '?ssl=true',
        role: getRoleFromUrl(databaseUrl),
        schemaName: instance.schemaName
    };

    return db.getConnector(newDBData).getClient().then(function (client)
    {
        return sqlGetter.cleanupSchema(newDBData).then(function (cleanupSchemaQuery)
        {
            return client.query(cleanupSchemaQuery);
        }).then(function ()
        {
            return sqlGetter.actualSchame();
        }).then(function (schemaStructure)
        {
            return client.query(schemaStructure).finally(client.done);
        });
    }).then(function ()
    {
        return db.getConnector(instance).getClient();
    }).then(function (client)
    {
        var query = squel.select().from('information_schema.tables').where('table_schema = $1').toString();
        return client.query(query, [instance.schemaName]).then(function (results)
        {
            function appendData(dumped, tableName)
            {
                return function (result)
                {
                    return dumped.concat([
                        {
                            name: tableName,
                            data: result.rows
                        }
                    ]);
                };
            }
            return Promise.reduce(_.map(results.rows, 'table_name'), function (dumped, tableName)
            {
                query = squel.select().from(tableName).toString();
                return client.query(query).then(appendData(dumped, tableName));
            }, []).then(function (dumped)
            {
                dump = dumped;
            });
        }).finally(client.done);
    }).then(function ()
    {
        return db.getConnector(newDBData).getClient();
    }).then(function (client)
    {
        var query;
        return Promise.reduce(dump, function (sum, table)
        {
            if(!table.data.length) {
                return;
            }
            query = squel.insert().into(table.name).setFieldsRows(table.data).toString();
            return client.query(query);
        }, 0).finally(client.done);
    }).then(function ()
    {
        return mainDB.getClient();
    }).then(function (client)
    {
        newDBData.databaseType = 'external';
        var query = squel.update().table('instances').setFields(SQLConverter.toUnderscore(newDBData)).where('id = $1').toString();
        return client.query(query, [instance.id]).finally(client.done);
    }).catch(SQLException.new);
}

module.exports = {
    get: get,
    create: create,
    checkMembership: checkMembership,
    setAsMember: setAsMember,
    invite: invite,
    getAllWhichUserBelongsTo: getAllWhichUserBelongsTo,
    getMembers: getMembers,
    getInvitations: getInvitations,
    checkAndRemoveInvitation: checkAndRemoveInvitation,
    changeDatabase: changeDatabase
};
