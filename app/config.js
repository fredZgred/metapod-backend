module.exports = {
    port: process.env.PORT || 3000,
    mainDBUrl: process.env.HEROKU_POSTGRESQL_GOLD_URL || 'postgres://metapod:metapod@localhost/metapod',
    instanceDBUrl: process.env.HEROKU_POSTGRESQL_RED_URL || 'postgres://metapod:metapod@localhost/metapod_instance',
    frontEndUrl: process.env.FRONTEND_URL || 'http://localhost:8080',
    applicationName: 'metapod',
    sendGrid: {
        apiKey: process.env.SENDGRID_APIKEY,
        noReplyAddress: 'no-reply'
    }
};
